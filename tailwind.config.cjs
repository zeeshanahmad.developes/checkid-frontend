/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
	theme: {
		container: {
			center: true
		},
		
		extend:{	
			margin:{
				'84':'84px'
			},
			fontSize:{
				'100xl':'100px'
			},
			height:{
				'400':'400px'
			},
			lineHeight:{
				'sixty':'60px',
				'122':'122px'
			},
			colors:{
				darkBlue:'#19236D',
				semiBlue:'#1434CB',
				black:'#0F0F0F',
				yellow: '#FFA800',
				semiLight:'#424D9F'								
			},
			container: {
				containerlg: '1160px'
			},
			backgroundColor:{
				lightgrey: '#F5F8FF',
			}
	}
		
	},
	plugins: [],
}
