export const companyData = {
    companyName:'Tabu Estates LTD',
    companyNumber:516021482,
    address:'Ramat Gan, Horgin #5,  5235605, Israel ',
    restrictions:'Limited',
    objectives:'To engage in any lawful business',
    status:'Active',
    typeOfOrganization:'Company',
    occupation:'Any occupation is legal',
    govt:'No',
    incorporation:'08-05-2019',
    phone:543309212,
    typeOfCorporation:'Israeli private company'
}
export const companyChanges = [
    {
        date: "28.11.2022",
        action: "Rename"
    },
    {
        date: "28.11.2021",
        action: "Registration of an official"
    },
    {
        date: "28.11.2020",
        action: "Registration of an official"
    },
    {
        date: "28.11.2019",
        action: "Rename"
    }
]
export const Estates = [
    {
        name:"Jenny Wilson",
        jobTitle:"Internet Security Assistant",
        phone:"(+33)7*******4",
        report:"Personal CheckId(tm) Information Report",
        comment:"This Person Might Belong To Another №15. Want to know which ones, order full report.",
    },
    {
        name:"Tabu Estates LTD",
        jobTitle:"www.tabuestates.com",
        phone:"777 66 44 64",
        report:"CheckId Report",
        comment:"This Company Might Have More Then (xx) child companies, for the full list, order Full CheckId(tm) business report."
    },
    {
        name:"Jenny Wilson",
        jobTitle:"Internet Security Assistant",
        phone:"(+33)7*******4",
        report:"Personal CheckId(tm) Information Report",
        comment:"This Person Might Belong To Another №15. Want to know which ones, order full report.",
    },
    {
        name:"Tabu Estates LTD",
        jobTitle:"www.tabuestates.com",
        phone:"777 66 44 64",
        report:"CheckId Report",
        comment:"This Company Might Have More Then (xx) child companies, for the full list, order Full CheckId(tm) business report."
    }
]
export const  Product =[
    {
        name:'Business Information Report',
        price:'99',
        ILS:'ILS',
        description:[
            {
                title:'General information about the company'
            },
            {
                title:'Shareholder Information'
            },
            {
                title:'Company Board Information'
            },
            {
                title:'Bond Information'
            },
            {
                title:'Contact Details'
            },
            {
                title:'Information from the tax authorities'
            },
            {
                title:'Information on restricted accounts at the Bank of Israel'
            },
            {
                title:'Legal Proceedings'
            },
            {
                title:'Business intelligence about affiliates'
            }
        ],
        report:'Ordering a checkid report',
        choice:1
    },
    {
        name:'Company extract',
        price:'45',
        ILS:'ILS',
        description:[
            {
                title:'General information about the company'
            },
            {
                title:'Shareholder Information'
            },
            {
                title:'Company Board Information'
            },
            {
                title:'Lien Information'
            },
            {
                title:'Official document of the Ministry of Justice '
            }
        ],
        report:'Ordering a registrar of companies report',
        choice:0
    }
]

export const proceedingData = [
    {
        caseId:'129-10-12',
        status:'Open',
        opening_date:'02-10-2022',
        case_nmae:'Tam v. Taboo Direct - Menha Line Ltd',
        case_description:'Supplier - customer',
        court:'Hello Petah Tikva',
        bag_type:'Small claim',
        prosecutors:[
            {
                title:'Amit Tam'
            },
            {
                title:'Amit Tam1'
            },
            {
                title:'Amit Tam2'
            }
        ],
        defendant:[
            {
                title:'Direct Taboo - Guide Line Ltd'
            },
            {
                title:'Direct Taboo1 - Guide Line Ltd'
            },
            {
                title:'Direct Taboo2 - Guide Line Ltd'
            }
        ]
    },
    {
        caseId:'129-10-12',
        status:'Open',
        opening_date:'02-10-2022',
        case_nmae:'Tam v. Taboo Direct - Menha Line Ltd',
        case_description:'Supplier - customer',
        court:'Hello Petah Tikva',
        bag_type:'Small claim',
        prosecutors:[
            {
                title:'Amit Tam'
            },
            {
                title:'Amit Tam1'
            },
            {
                title:'Amit Tam2'
            }
        ],
        defendant:[
            {
                title:'Direct Taboo - Guide Line Ltd'
            },
            {
                title:'Direct Taboo1 - Guide Line Ltd'
            },
            {
                title:'Direct Taboo2 - Guide Line Ltd'
            }
        ]
    },
    {
        caseId:'129-10-12',
        status:'Open',
        opening_date:'02-10-2022',
        case_nmae:'Tam v. Taboo Direct - Menha Line Ltd',
        case_description:'Supplier - customer',
        court:'Hello Petah Tikva',
        bag_type:'Small claim',
        prosecutors:[
            {
                title:'Amit Tam'
            },
            {
                title:'Amit Tam1'
            },
            {
                title:'Amit Tam2'
            }
        ],
        defendant:[
            {
                title:'Direct Taboo - Guide Line Ltd'
            },
            {
                title:'Direct Taboo1 - Guide Line Ltd'
            },
            {
                title:'Direct Taboo2 - Guide Line Ltd'
            }
        ]
    }
]

export const additionalReport = [
    {
        name:'Locate company owners',
        price:'11',
        ILS:'ILS',
        description:[
            {
                title:'Business data summary'
            },
            {
                title:'Related Companies'
            }
        ],
        report:'Ordering a registrar of companies report'
    },
    {
        name:'Certificate of Incorporation',
        price:'79',
        ILS:'ILS',
        description:[
            {
                title:'Order of a copy of a certificate of incorporation signed by the Corporations Authority'
            }
        ],
        report:'Ordering a registrar of companies report'
    },
    {
        name:'Locate company owners',
        price:'468',
        ILS:'ILS',
        starting_from:'/starting from',
        description:[
            {
                title:'Business data summary'
            },
            {
                title:'Related Companies'
            }
        ],
        report:'Company extract in english'
    },
    {
        name:'Company file',
        price:'144',
        ILS:'ILS',
        description:[
            {
                title:'Ordering a company file that includes copies of all company documents submitted to the Corporations Authority.'
            }
        ],
        report:'Ordering a registrar of companies report'
    }
]
export const relatedProducts=[
    {
        name:'best choice',
        sub_title:'Business Information Report ',
        price:'99',
        ILS:'ILS',
        report:'Ordering a checkid report',
        description:[
            {
                title:'General information about the company'
            },
            {
                title:'Shareholder Information',
            },
            {
                title:'Company Board Information'
            },
            {
                title:'Information from the tax authorities'
            },
            {
                title:'Information on restricted accounts at the Bank of Israel'
            },
            {
                title:'Legal Proceedings'
            },
            {
                title:'Business intelligence about affiliates'
            }
        ]
    },
    {
        name:'Company extract',
        sub_title:'',
        price:'45',
        ILS:'ILS',
        report:'Ordering a checkid report',
        description:[
            {
                title:'General information about the company'
            },
            {
                title:'Shareholder Information',
            },
            {
                title:'Company Board Information'
            },
            {
                title:'Information from the tax authorities'
            }
        ]
    }
]